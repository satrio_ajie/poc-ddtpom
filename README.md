# README #

This branch contains refactor from branch DEV repository bos_revamp_web_rf branch dev for proof of concept implementation Page Object Model (POM) and Data Driven Test in test scenarios for web application. 

## Prerequisites
1. Get an access to Shipper's VPN Staging ([go here](https://shipperid.atlassian.net/wiki/spaces/ID/pages/80216397/How+to+Request+Access+Example))
2. Install common dependencies:
   1. Install [python - 3.8](https://www.python.org/downloads/).
   2. Install dependency that is listed in file requirements.txt 
        ```
        pip install -r requirements.txt 
        ```
3. Install web testing dependencies:
    1. Download chromedriver (For running web automation on chrome browser).
        ```
        https://chromedriver.chromium.org/downloads
        ```
    2. Download geckodriver (For running web automation on firefox browser).
        ```
        https://github.com/mozilla/geckodriver/releases
        ```
    3. Put downloaded chromedriver and geckodriver into folder, example :
        ```
        /Users/<user_folder>/BrowserDriver
        ```
    4. Add folder into PATH environment variable, example for linux/mac user :
        ```
        export PATH="/path/to/BrowserDriver:$PATH"
        ```
       
## Structure
### Folder Structure
```
    libraries :
    resources :
        backend 
        common 
        page_object 
    result:
    test:
```

### Description
1. **libraries** : Main library and static global parameters for project. Treated as resource file for other robot files. Containing conf.tmpl for getting parameter from vault svc. Can be used for storing native python code if needed
2. **resources** : keyword for robot framework
    1. backend : maintaining  keywords for access Back End Application and Databases
    2. common : maintaining common keywords that have not interaction with web element and back end or DB application (ex: random number generator, random logistic id generator, etc)
    3. page_object  :  maintaining keywords for Page Object Model and Page Factory
3. **test** : contain test case and test scenario in gherkin language 
4. **result** : contain test result from test execution 
    
## Naming Convention
### File Naming
1. Use snake_case for file and folder naming
2. for page object model file : `<page_name>_page.<file_extension>` . ex:
    * login_page.robot
    * cashless_marketplace_page.robot
    
### Variable Convention:
1. All variable name using snake_case
   
### File Structure Convention for libraries Folder:
1. All library that will be used by the program should be listed under Settings section in library.robot file, ex:
    ```
    *** Settings ***
    Library         SeleniumLibrary     timeout=20      run_on_failure=SeleniumLibrary.Capture Page Screenshot
    Library         String
    Library         DateTime
    
    ```
2. Browser type should be defined under Variable section  in library.robot file, ex:
    ```
    *** Variables ***
    ${browser}                  chrome/headlesschrome/firefox/headlessfirefox
    
    ...
    
    ```
3. if there are some native python code, file should be put inside folder with package name inside <project_root>/libraries/ folder, ex: `<project_root>/libraries/<python_package_name>/<file_name>.py`. The python file should be listed in Settings section as Library in library.robot file, ex:
    ```
    *** Settings ***
    Library          ${EXECDIR}/libraries/<python_package_name>/<file_name>.py

    ```

### File Structure Convention for Resources Folder:
### common Folder
1. common folder will only be used  for stored robot files that contain keyword that have not interaction with web element and back end or DB application (ex: random number generator, random logistic id generator, etc)
2. all common file should be use `<project_root>/libraries/libray.robot` file under Setting section, ex:
    ```
    *** Settings ***
    Resource        ${EXECDIR}/libraries/library.robot
    ```
3. all file under common folder should be listed as `Resource`under Setting section in `<project_root>/resources/page_object/pom.robot` file, ex:
    ```
    *** Settings ***
    Resource        ${EXECDIR}/resources/common/common_keyword.robot
    ```
4. all keyword should not be written in gherkin language format and should not be started with Given , When , Then , And , or But words, ex:
    ```
    *** Keywords ***
    Set id Logistic for order domestic
    ...
   
    ```

### back_end Folder
1. backend folder will only be used  for stored robot files that contain keyword for access Back End Application and Databases
2. all database interaction keyword should be put inside `<project_root>/resources/back_end/database.robot` file
3. all back end related interaction keyword should be put inside `<project_root>/resources/back_end/back_end_keyword.robot` file or specificly can be group inside directory under `<project_root>/resources/back_end/` folder, ex:
    ```
    <project_root>
        /resources
            /back_end
                /accountsvc
                /walletsvc
                ...
    
    ```
4. all file under back_end folder should be listed as `Resource`under Setting section in `<project_root>/resources/page_object/pom.robot` file, ex:
    ```
    *** Settings ***
    Resource        ${EXECDIR}/resources/back_end/back_end_keyword.robot
    ```
5. all keyword should not be written in gherkin language format and should not be started with Given , When , Then , And , or But words, ex:
    ```
    *** Keywords ***
    Start accountsvc DB Connection 
    ...
   
    ```
   
### page_object Folder
1. page object folder will only be used  for stored robot files that contain keywords for Page Object Model and Page Factory. All Keyword should have in interaction with web element in web application 
2. page file should have name with format `<page_name>_page.robot` for representing actual web page in actual web application, ex: 
   ```
    login_page.robot
    cashless_marketplace_page.robot
   ``` 
3. all file page file inside page_object folder should be listed as `Resource`under Setting section in `<project_root>/resources/page_object/pom.robot` file, ex:
    ```
    *** Settings ***
    Resource        ${EXECDIR}/resources/page_object/login_page.robot
    Resource        ${EXECDIR}/resources/page_object/cashless_marketplace_page.robot
    ```
4. each page file inside page_object folder contain 3 section 
    1. Setting section should  contain library.robot file as a resource 
        ```
            *** Settings ***
            Resource        ${EXECDIR}/libraries/library.robot
        ```
    2. Variable section should contain variable that represent web element in actual web page in actual web application based on page represented by the page file (Page Factory), ex: In file login_page.robot (which represent login page)
        ```
            *** Variables ***
            ${login_page_title}                         id:loginTitle           #represent web element login page title in login page
            ${txt_login_email}                          id:emailAddress         #represent web element textbox for email in login page
       
            ...
        ```
    3. Keywords section contain keyword for doing interaction with web element that listed in variable section. All keyword should not be written in gherkin language format and should not be started with Given , When , Then , And , or But words. All keyword cannot contain other than standard keywords which are owned by the robot framework and selenium library.
        ```
        *** Keywords ***
        User inputs email and password on login page
        [Arguments]     ${email}      ${password}
        Wait Until Element Is Visible       ${txt_login_email}
        Wait Until Element Is Visible       ${txt_login_password}
        Input Text  ${txt_login_email}  ${email}
        Input Text  ${txt_login_password}  ${password}
        ```
### test Folder
1. Test Folder only contain test case and test scenario in gherkin language 
2. Test Folder should contain `<project_root>/resources/page_object/pom.robot` as Resource,  `Open Boss Web` as Test Setup , and 
    ```
    *** Settings ***
    Resource        ${EXECDIR}/resources/page_object/pom.robot
    ```
3. Example structure for data driven test to represent multiple sample data using same scenario
    ```
        *** Settings ***
        Documentation   Valid Login
        Resource        ${EXECDIR}/resources/page_object/pom.robot
        
        Force Tags      validLogin  
        Test Setup      Open Boss Web
        Test Teardown   Close All Browsers
        Test Template   Valid Login Merchant
        
        *** Variables ***
        
        *** Test Cases ***           email                 password
        send valid merchant postpay  dessy+777@shipper.id  0987654321
        send valid merchant cash     dessy+555@shipper.id  12345678
        
        *** Keywords ***
        Valid Login Merchant
            [Arguments]    ${email}  ${password}
            [Documentation]     Scenario for Valid Login Merchant
            Given Login page displayed in Boss web
            When User inputs email and password on login page  ${email}  ${password}
            And User clicks masuk button on login page
            Then The dashboard will be displayed
    ```

## How to Run Locally
1. navigate into project root folder 
2. execute command to download parameter from vault svc 
   #### for Linux/MacOs 
   ```
   chmod +x get_conf.sh 
   ./get_conf.sh
   ```
3. execute command using `robot` command
   
    #### Run Specific Robot File
    ```
    robot -d ./result --variable browser:chrome <file_path>/<file_name>.robot
    ```
    #### Run Robot Specific Tags
    ```
    robot -d ./result --include tagName --variable browser:chrome <file_path>/<folder_name>
    ```
    #### Run Specific Test Case from a Test Suite
    ```
    robot -d ./result -t "test case name here" --variable browser:chrome <file_path>/<file_name>.robot
    ```
