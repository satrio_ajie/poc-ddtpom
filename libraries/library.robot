*** Settings ***
Library         SeleniumLibrary     timeout=20      run_on_failure=SeleniumLibrary.Capture Page Screenshot
Library         String
Library         DateTime

Variables       ${EXECDIR}/libraries/conf/conf.yaml

*** Variables ***
${environment}              dev     #possible values: dev OR staging OR prod
${browser}                  chrome      #possible values: chrome OR headlesschrome OR firefox OR headlessfirefox
${ss_dir}                   ${EXECDIR}/result

#URL for Boss Web App
#&{baseUrl}                  dev=https://bos.staging.shipper.id
#...                         staging=https://bos.staging.shipper.id
#...                         sandbox=https://bos.sandbox.shipper.id
#...                         prod=https://bos.staging.shipper.id
${baseUrl}                  ${base_url_service}