*** Settings ***
Resource        ${EXECDIR}/libraries/library.robot

*** Keywords ***
Set id Logistic for order domestic
    [Arguments]     ${logistic_name}    ${rate_type}
    ${rate_name}=	Set Variable     ${logistic_name}
    ${id_rate}=	Replace String     ${rate_name}      ${Space}-${Space}    -
    ${id_rate}=	Replace String     ${id_rate}      ${Space}      -
    ${id_logisticRate}=     Set Variable	${rate_type}_${id_rate}
    [Return]            ${id_logisticRate}

Create Random AWB Number on Buat Pesanan Cashless
    [Arguments]         ${marketplace}  ${logistic}
    ${randomAWB}=        Generate Random String      6      [NUMBERS]
    ${randomAWB}=        Convert To Integer          ${randomAWB}
    ${awbNumber}=        Set Variable    ${logistic}-${randomAWB}
    &{result}=          Create Dictionary       marketplace=${marketplace}      logistic=${logistic}    awbNumber=${awbNumber}
    [Return]            ${awbNumber}

Generate Valid Data For Email and Phone
    ${randomNumber}=    Generate Epoch Timestamp
    ${email}=           Set Variable    qa+merchant_${randomNumber}@shipper.id
    ${randomPhone}=        Generate Random String      11      [NUMBERS]
    ${randomPhone}=        Convert To Integer          ${randomPhone}
    ${phone}=               Set Variable    08${randomPhone}
    &{result}=          Create Dictionary       email=${email}      phone=${phone}
    [Return]            ${result}

Generate Epoch Timestamp
    ${d}=       Get Current Date
    ${d}=       Convert Date    ${d}    epoch
    ${d}=       Convert To String           ${d}
    ${d}=       Remove String               ${d}    -   :   .   ${SPACE}
    ${result}=      Convert To String           ${d}
    Log             ${result}
    [return]        ${result}