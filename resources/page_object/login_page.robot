*** Settings ***
Resource        ${EXECDIR}/libraries/library.robot

*** Variables ***
${login_page_title}                         id:loginTitle
${txt_login_email}                          id:emailAddress
${txt_login_password}                       id:passwordUser
${btn_login_masuk}                          id:btn-login-submit
${link_forgot_password}                     id:link-forgot-password
${link_to_register}                         id:link-to-register
${warning_email}                            id:emailAddressErrorMassage
${lnk-daftarSekarang}                               id:link-to-register

*** Keywords ***
Login page displayed in Boss web
    ${title}  Get Text    ${login_page_title}
    Should Be Equal As Strings    Masuk ke dalam akun Anda      ${title}

User inputs email and password on login page
    [Arguments]     ${email}      ${password}
    Wait Until Element Is Visible       ${txt_login_email}
    Wait Until Element Is Visible       ${txt_login_password}
    Input Text  ${txt_login_email}  ${email}
    Input Text  ${txt_login_password}  ${password}

User clicks masuk button on login page
    Click Button  ${btn_login_masuk}

Display warning message on login page
    [Arguments]         ${error_msg}
    Sleep               3s
    ${warning_email_text}  Get Text    ${warning_email}
    Wait Until Element Is Visible  ${warning_email}
    Should Be Equal As Strings    ${warning_email_text}      ${error_msg}

Click Link Daftar Sekarang on login page
    Run Keyword And Continue on Failure     Click Element      ${lnk-daftarSekarang}
