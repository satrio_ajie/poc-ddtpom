*** Settings ***
Resource        ${EXECDIR}/libraries/library.robot

Resource        ${EXECDIR}/resources/common/common_keyword.robot

Resource        ${EXECDIR}/resources/page_object/login_page.robot
Resource        ${EXECDIR}/resources/page_object/dashboard_page.robot

*** Keywords ***
Open Browser And Go To URL
    [Arguments]         ${go_to_url}
    Open Browser        url=${go_to_url}        browser=${browser}
    Set Screenshot Directory        ${ss_dir}

Open Boss Web
    Open Browser And Go To URL      ${baseUrl.shipper_bos_web}
    Maximize Browser Window

