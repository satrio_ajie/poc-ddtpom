*** Settings ***
Documentation   Valid Login
Resource        ${EXECDIR}/resources/page_object/pom.robot

Force Tags      validLogin  disabled
Test Setup      Open Boss Web
Test Teardown   Close All Browsers
Test Template   Valid Login Merchant

*** Variables ***

*** Test Cases ***           email                 password
send valid merchant postpay  dessy+777@shipper.id  0987654321
send valid merchant cash     dessy+555@shipper.id  12345678

*** Keywords ***
Valid Login Merchant
    [Arguments]    ${email}  ${password}
    [Documentation]     Scenario for Valid Login Merchant
    Given Login page displayed in Boss web
    When User inputs email and password on login page  ${email}  ${password}
    And User clicks masuk button on login page
    Then The dashboard will be displayed